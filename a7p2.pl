dates(feb20).
dates(may15).
dates(may31).
dates(june23).
dates(aug29).

person(bryce).
person(donald).
person(jane).
person(sawyer).
person(walter).

crazes(dancingBaby).
crazes(explodingWhale).
crazes(lolcat).
crazes(rickrolling).
crazes(willItBlend).

gifts(blender).
gifts(coffeeMaker).
gifts(diningTable).
gifts(juicePress).
gifts(microwave).



solve:- person(P1), person(P2), person(P3), person(P4), person(P5),
        unique([P1,P2,P3,P4,P5]),

    crazes(C1), crazes(C2), crazes(C3), crazes(C4), crazes(C5),
    unique([C1,C2,C3,C4,C5]),

    gifts(G1), gifts(G2), gifts(G3), gifts(G4), gifts(G5),
    unique([G1,G2,G3,G4,G5]),

    Solution = [[feb20, P1, C1, G1],
                [may15, P2, C2, G2],
                [may31, P3, C3, G3],
                [june23, P4, C4, G4],
                [aug29, P5, C5, G5]],

%1. Bryce went on vacation after Sawyer.
\+    (
      member([feb20, bryce, _,_], Solution);
      member([aug29, sawyer,_,_], Solution)
      ),
%2. The 5 people were: the person who started the rickrolling craze, the vacationer who left on May 31, the person who
%received the dining table, Walter, and the person who started the lolcat craze.
\+    (
      member([may31, _, rickrolling,_], Solution);
      member([_, _, rickrolling,diningTable], Solution);
      member([_, walter, rickrolling,_], Solution);

      member([may31, _, lolcat,_], Solution);
      member([may31, walter, _,_], Solution);
      member([may31, _,_,diningTable], Solution);

      member([_, walter,_,diningTable], Solution);
      member([_, _,lolcat,diningTable], Solution);

      member([_, walter,lolcat,_], Solution)
      ),
%3. The person who received the microwave is Bryce.
member([_, bryce,_,microwave], Solution),
%4. Jane went on vacation after the person who received the juice press.
\+    (
        member([feb20, jane, _,_], Solution);
        member([aug29,_,_,juicePress], Solution);
        member([_,jane,_,juicePress], Solution)
      ),
%5. Of Walter and the person who started the dancing baby craze, one left for vacation on August 29 and the other loved
%the microwave they received.
\+ member([_, walter, dancingBaby,_], Solution),
member([_,bryce, dancingBaby,_], Solution),
member([aug29,walter,_,_], Solution),
%6. The person who started the exploding whale craze is Walter.
member([_,walter, explodingWhale,_], Solution),
%7. The person who received the juice press went on vacation before the person who received the coffee maker.
\+    (
      member([feb20, _, _,coffeeMaker], Solution);
      member([aug29, _,_,juicePress], Solution)
      ),
%8. Either the vacationer who left on February 20 or the vacationer who left on August 29 started the will it blend craze.
member([Possibility,_,_, willItBlend], Solution), (Possibility = feb20; Possibility = aug29),
%9. The person who received the juice press didn't start the lolcat craze.
\+ member([_,_,lolcat, juicePress], Solution),
%10. The person who received the coffee maker is not Jane.
\+ member([_,jane,_, coffeeMaker], Solution),
%11. The person who started the rickrolling craze is not Sawyer.
\+ member([_,sawyer,rickrolling, _], Solution),

            writer(feb20, P1, C1, G1),
            writer(may15, P2, C2, G2,
            writer(may31, P3, C3, G3),
            writer(june23, P4, C4, G4),
            writer(aug29, P5, C5, G5).

writer(W,X,Y,Z):- write(X), write(' vacationed on '), write(W), write(' and received a  '), write(Z), write(' as a wedding gift, and started a '), write(Y), write(' craze'), nl.

unique([H | T]) :- member(H, T), !, fail.
unique([_ | T]) :- unique(T).
unique([_]).
