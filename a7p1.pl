placement(fourth).
placement(eighth).
placement(ninth).
placement(tenth).
placement(eighteenth).

firstname(asher).
firstname(ashlyn).
firstname(casey).
firstname(mckenna).
firstname(tony).

penpal(argentinean).
penpal(jordanian).
penpal(norwegian).
penpal(palauan).
penpal(zimbabwean).

lastname(avila).
lastname(gillespie).
lastname(mercado).
lastname(rosa).
lastname(vaughan).

logicPuzzle1() :-
firstname(F1), firstname(F2), firstname(F3), firstname(F4), firstname(F5),
%\+(F1 = F2;F1 = F3;F1 = F4;F1 = F5;F2 = F3;F2 = F4;F2 = F5;F3 = F4;F3 = F5;F4 = P5),
unique([F1,F2,F3,F4,F5]),

penpal(Pen1), penpal(Pen2), penpal(Pen3), penpal(Pen4), penpal(Pen5),
%\+(Pen1 = Pen2;Pen1 = Pen3;Pen1 = Pen4;Pen1 = Pen5;Pen2 = Pen3;Pen2 = Pen4;Pen2 = Pen5;Pen3 = Pen4;Pen3 = Pen5;Pen4 = Pen),
unique([Pen1,Pen2,Pen3,Pen4,Pen5]),

lastname(L1), lastname(L2), lastname(L3), lastname(L4), lastname(L5),
%\+(L1 = L2;L1 = L3;L1 = L4;L1 = L5;L2 = L3;L2 = L4;L2 = L5;L3 = L4;L3 = L5;L4 = L5),
unique([L1,L2,L3,L4,L5]),

Solution = [[fourth, F1, Pen1, L1],
            [eigth, F2, Pen2, L2],
            [ninth, F3, Pen3, L3],
            [tenth, F4, Pen4, L4],
            [eighteenth, F5, Pen5, L5]],

% 1. The person in fourth place has Avila as a surname.
member([fourth,_,_, avila], Solution),
% 2. The person whose last name is Gillespie doesn't have the Norwegian penpal.
\+ member([_,_, norwegian, gillespie], Solution),
% 3. The 5 people were the person whose last name is Mercado, the person with the Palauan penpal, the person in eighteenth place, Asher, and the person whose last name is Avila.
\+
(
  member([eighteenth,asher,_,_], Solution);
  member([eighteenth,_,_,mercado], Solution);
  member([eighteenth,_,_,avila], Solution);
  member([eighteenth,_,palauan,_], Solution);

  member([_,asher,_,mercado], Solution);
  member([_,asher,palauan,_], Solution);
  member([_,asher,_,avila], Solution);

  member([_,_,palauan,mercado], Solution);
  member([_,_,palauan,avila], Solution)
),
% 4. The person whose last name is Rosa doesn't have the Argentinean penpal.
\+ member([_,_,argentinean, rosa], Solution),
% 5. The person whose last name is Mercado finished after the person with the Argentinean penpal.
\+ (
      member([fourth,_,_, mercado], Solution);
      member([eighteenth,_, argentinean,_], Solution);
      member([_,_, argentinean, mercado], Solution)
   ),
 % 6. Mckenna finished before the person whose last name is Mercado.
 \+ (
      member([eighteenth, mckenna,_,_], Solution);
      member([_, mckenna, _, mercado], Solution)
    ),
% 7. Either the person whose last name is Avila or the person whose last name is Gillespie has the Norwegian penpal.
member([_,_, norwegian, Potential], Solution), (Potential, [avila, gillespie]),
% 8. The person in ninth place does not have Vaughan as a last name.
\+ member([ninth,_,_, vaughan], Solution),
% 9. The person with the Jordanian penpal is Casey.
member([_,casey, jordanian,_], Solution),
% 10. Ashlyn finished before the person with the Palauan penpal.
\+ (
      member([eighteenth, ashlyn,_,_], Solution);
      member([fourth,_, palauan,_], Solution);
      member([_, ashlyn, palauan,_])
   ),
% 11. Of Tony and Mckenna, one came in tenth place and the other has Vaughan as a surname.
member([tenth, Possibility,_,_], Solution), (Possibility = tony; Possibility = mckenna),
% Using #8 and #11, Neither Tony nor Mckenna came in ninth
\+ (
      member([tenth,_,_, vaughan], Solution);
      member([ninth, mckenna,_,_], Solution);
      member([ninth, tony,_,_], Solution)
   ),

writer(fourth, F1, Pen1, L1),
writer(eigth, F2, Pen2, L2),
writer(ninth, F3, Pen3, L3),
writer(tenth, F4, Pen4, L4),
writer(eighteenth, F5, Pen5, L5).

writer(A,B,C,D) :- write(B), write(', '), write(D), write(' came in '), write(A), write(' and has a '), write(C), write(' penpal'), nl.

unique([H | T]) :- member(H, T), !, fail.
unique([_ | T]) :- unique(T).
unique([_]).
